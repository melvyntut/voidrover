# Void Rover

These are the files for a 2D side-scroller game called Void Rover.

## Installation

Before running the game, you first need to install Processing, which is a free and open-source IDE. It can be downloaded at https://processing.org/download

## Usage

Once Processing is installed, go to the folder where you downloaded the game files to. Double-click on the file named "voidrover". The program's main code as well as the code for the "ship and "asteroid" objects should open in Processing. Click the play button in Processing to run the game.

## Controls

Use your 'a' key to move the ship up and your 'z' to move the ship down. Press 'r' at any time to restart the level.

## Modes

There are three game modes: easy, medium, hard. Easy mode generates three asteroids at a time. Medium generates five at a time. Hard generates five also, but the asteroids move faster. Once you have selected a level, you can only play on that level even if your press the 'r' button. If you want to select a new level, you'll have to close the window and run the game again.
