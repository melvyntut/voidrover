class Ship {
  
  int fill = 255;
  
  float x = width/8;
  float y = height/2;
  float ychange = 0;
  float w = 100;
  float h = w/2;
  
  boolean areWeHit = false;
  int hits = 0;
  int hitLimit = 3;
  
  void show(){
    fill(fill);
    rectMode(CENTER);
    rect(x, y, w, h);
    y += ychange;
    y = constrain(y, h + h/2, height - (h + h/2));
  }
  
  void move(int steps){
    ychange = steps;
  }
  
  void checkCollision(Asteroid asteroid){
    if(asteroid.x < x && y < (asteroid.y + h) && y > (asteroid.y - asteroid.h)){
      //reposition asteroid
      asteroid.reposition();
      //add one to hits variable
      hits += 1;
      print(hits + " ");
      areWeHit = true;
    }
  }
  
  boolean destroyed(){
    if(hits == hitLimit){
      return true;
    }
    return false;
  }
  
}
