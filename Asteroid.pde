class Asteroid{
  
  float w = 50;
  float h = w;
  float x = random(width - w, (width + w) + 1200);
  float xspeed = -20;
  float y = random(w/2, height - w/2);
  int fill = 153;
  
  void show(){
    //gives colour, mode, and position
    fill(fill);
    rectMode(CENTER);
    rect(x, y, w, h);
    //if asteroid moves off-screen, reposition
    if(x < 0 - w){
      reposition();
    }
  }
  
  void reposition(){
    x = random(width + w, (width + w) + 1200);
    y = random(w/2, height - w/2);
  }
  
  void update(){
    //speed of asteroid
    x += xspeed;
  }
  
}
