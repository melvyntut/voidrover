import processing.sound.*;
SoundFile main;
SoundFile gameover;

String audioName = "main.wav";
String audioName2 = "gameover.wav";
String path;
String path2;

Ship ship;
Asteroid[] asteroid;

boolean easy = false;
boolean medium = false;
boolean hard = false;
boolean hidden = false;
boolean gameStarted = false;

boolean playOnce;

String intro = "Press 1 for easy, 2 for medium, 3 for hard";
String intro2 = "Use 'a' to move up and 'z' to move down";

int time;

int score;
int previousScore = 0;

void setup(){
  size(1200, 800);
  frameRate(60);
  ship = new Ship(); 
  asteroid = new Asteroid[5];
  
  time = 0;
  
  path = sketchPath(audioName);
  path2 = sketchPath(audioName2);
  
  main = new SoundFile(this, path);
  gameover = new SoundFile(this, path2);
  
  main.amp(1);
  main.loop();
  gameover.amp(1);
  
  playOnce = true;
  
  println(main.isPlaying());
  
  for(int i = 0; i < asteroid.length; i++){
    asteroid[i] = new Asteroid();
  }

}

void draw(){
  background(0);
  
  //println(previousScore);
  
  fill(255);
  textSize(32);
  textAlign(CENTER);
  text((intro), width/2, height/2);
  text((intro2), width/2, height/2 + 40);
  text(time, width - (width/16), height/16);
  
  if(easy){
    for(int i = 0; i < 3; i++){
      asteroid[i].show();
      asteroid[i].update();
      ship.checkCollision(asteroid[i]);
      intro = "";
      intro2 = "";
      text(("easy mode"), width/2, height/16);
      gameStarted = true;
    }
  } else if(medium){
    for(int i = 0; i < 5; i++){
      asteroid[i].show();
      asteroid[i].update();
      ship.checkCollision(asteroid[i]);
      intro = "";
      intro2 = "";
      text(("medium mode"), width/2, height/16);
      gameStarted = true;
    }
  } else if(hard){
    for(int i = 0; i < 5; i++){
      asteroid[i].show();
      asteroid[i].update();
      asteroid[i].xspeed = -30;
      ship.checkCollision(asteroid[i]);
      intro = "";
      intro2 = "";
      text(("hard mode"), width/2, height/16);
      gameStarted = true;
    }
  } /*else if(hidden){
    for(int i = 0; i < 2; i++){
      background(255);
      ship.fill = 0;
      intro = "";
      intro2 = "";
      text(("hidden mode"), width/2, height/16);
      gameStarted = true;
    }
  }*/
  
  if(!ship.destroyed() && gameStarted){
    if(frameCount % 60 == 0){
        time += 1;
    }
  }
  
  ship.show();
  ship.destroyed();
  
  //displays number of hits and time passed
  fill(255);
  textSize(32);
  textAlign(CENTER);
  text(("Hits: " + ship.hits), width/16, height/16);
  
  //if the ship is destroyed, remove objects from screen, game over
  if(ship.destroyed()){
    ship.x = -1000;
    for(int i = 0; i < asteroid.length; i++){
      asteroid[i].x = 2000;
    }
    
    main.stop();
    if(playOnce){
      gameover.play();
      playOnce = false;
    }
    
    fill(255);
    textSize(32);
    textAlign(CENTER);
    text("GAME OVER", width/2, height/2 - 40);
    if(time > previousScore){
      previousScore = time;
    }
    text("High score: " + previousScore, width/2, height/2);
    text("Press 'r' to start again", width/2, height/2 + 40);
  }
}

void keyReleased(){
  ship.move(0);
}

void keyPressed(){
  if(key == 'a'){
    ship.move(-10);
  } else if(key == 'z'){
    ship.move(10);
  } else if(key == 'r'){
    reset();
  } else if(key == '1'){
    easy = true;
  } else if(key == '2'){
    medium = true;
  } else if(key == '3'){
    hard = true;
  } else if(key == 'h'){
    hidden = true;
  }
}

void reset(){
  main.stop();
  println(main.isPlaying());
  frameCount = -1;
}
